package com.lambda.aws.lambdaAwsExample;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
   	
    	// (1) Define the AWS Region in which the function is to be invoked
    	Regions region = Regions.fromName("us-east-1");
    	
    	// enter creds here
    	BasicAWSCredentials credentials = new 
    	  BasicAWSCredentials("Access key", "secret key");
    	
    	// (2) Modify to leverage credentials
    	AWSLambdaClientBuilder builder = AWSLambdaClientBuilder.standard()
    	  .withCredentials(new AWSStaticCredentialsProvider(credentials))                                    
    	  .withRegion(region);
    	
    	// (3) Build the client, which will ultimately invoke the function
    	AWSLambda client = builder.build();
    	// (4) Create an InvokeRequest with required parameters
    	InvokeRequest req = new InvokeRequest()
    	                           .withFunctionName("HelloWorld");
    	                           /*.withPayload("{ ... }");*/ // optional
    	// (5) Invoke the function and capture response
    	InvokeResult result = client.invoke(req);
    	ByteBuffer buffer = result.getPayload();
    	// (6) Handle result
    	try {
			System.out.println(new String(buffer.array(), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
